import random

import tensorflow as tf
import matplotlib.pyplot as plt
import numpy as np

IMG_SIZE = 56
HIDDEN_L_SIZE = 2002
MIX_SCALE = 100000

# (x_train, y_train), (x_test, y_test) = tf.keras.datasets.mnist.load_data()
x_train2d = np.load('x_train' + str(IMG_SIZE) + '.npy')
y_train = np.load('y_train' + str(IMG_SIZE) + '.npy')

# mix train dataset
for i in range(0, MIX_SCALE):
    r1 = int(random.random()*len(x_train2d))
    r2 = int(random.random()*len(x_train2d))
    tempx = np.copy(x_train2d[r1])
    tempy = np.copy(y_train[r1])
    x_train2d[r1] = np.copy(x_train2d[r2])
    y_train[r1] = np.copy(y_train[r2])
    x_train2d[r2] = tempx
    y_train[r2] = tempy

# reshape so that its correct for network input - shape(*, IMG_SIZE, IMG_SIZE)
x_train = np.zeros((len(x_train2d), IMG_SIZE, IMG_SIZE))
for i in range(0, len(x_train2d)):
    x_train[i][:, :] = x_train2d[i].reshape(IMG_SIZE, IMG_SIZE)

for i in range(0, len(y_train)):
    y_train[i] = y_train[i] - 1



x_train = x_train / 255


# build the model
model = tf.keras.Sequential([
    tf.keras.layers.Flatten(input_shape=(IMG_SIZE, IMG_SIZE)),
    tf.keras.layers.Dense(HIDDEN_L_SIZE, activation=tf.nn.relu),
    tf.keras.layers.Dropout(0.5),
    tf.keras.layers.Dense(500, activation=tf.nn.relu),
    tf.keras.layers.Dropout(0.5),
    tf.keras.layers.Dense(500, activation=tf.nn.relu),
    tf.keras.layers.Dropout(0.5),
    tf.keras.layers.Dense(26, activation=tf.nn.softmax)
])


model.compile(optimizer='adam',
              loss='sparse_categorical_crossentropy',
              metrics=['accuracy'])

model.fit(x_train, y_train.transpose(), epochs=15)

test_loss, test_acc = model.evaluate(x_train, y_train)

print('Test accuracy:', test_acc)

model.save('model' + str(HIDDEN_L_SIZE) + 'x' + str(IMG_SIZE) + '.h5')  # creates a HDF5 file
del model  # deletes the existing model

