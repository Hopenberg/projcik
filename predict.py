from PIL import Image
import numpy as np
import sys
import tensorflow as tf
import matplotlib.pyplot as plt

IMG_SIZE = 56
HIDDEN_L_SIZE = 2002

# function that loads image and converts it to array IMG_SIZExIMG_SIZE
def load_image(infilename):
    img = Image.open(infilename)
    img = img.resize((IMG_SIZE, IMG_SIZE), Image.ANTIALIAS)
    img.load()
    data = np.asarray(img, dtype="int32")
    data2 = data[:, :, 0]
    return 1 - (data2/255)


# create ditionary of letters A - Z mapped to 0 - 25
char = 'A'
labels = dict()
for i in range(26):
    labels[i] = char
    char = chr(ord(char) + 1)  # get next letter


try:

    img_name = sys.argv[1]
    img_array = load_image(img_name)
    img_array[img_array < 0.4] = 0  # make background pixels equal 0
    model = tf.keras.models.load_model('model' + str(HIDDEN_L_SIZE) + 'x' + str(IMG_SIZE) + '.h5')
    pre = model.predict(np.array([img_array]))

    plt.subplot(211)
    plt.bar(labels.values(), pre[0])
    plt.subplot(212)
    plt.imshow(img_array)
    plt.show()

    print('Predicted:', labels[int(np.argmax(pre[0]))])
except:
    print('Something went wrong...', sys.exc_info())
















