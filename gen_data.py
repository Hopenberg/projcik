from PIL import Image
import numpy as np
import os

IMG_SIZE = 28

def load_image(infilename) :
    img = Image.open(infilename)
    img = img.resize((IMG_SIZE, IMG_SIZE), Image.BILINEAR)
    img.load()

    data = np.asarray( img, dtype="int32" )
    data2 = data[:, :, 3]
    return data2


def save_image( npdata, outfilename ) :
    img = Image.fromarray( np.asarray( np.clip(npdata,0,255), dtype="uint8"), "L" )
    img.save( outfilename )


x_train = np.zeros((1, IMG_SIZE*IMG_SIZE))  # train examples
y_train = np.zeros(0)
label_num = 0
rootDir = './Latin'

for dirName, subdirList, fileList in os.walk(rootDir):
    print('Im in: ' + dirName)
    if fileList:
        label_num += 1
    for fname in fileList:
        im = load_image(dirName + '/' + fname)
        x_train = np.append(x_train, [im.ravel()], axis=0)
        y_train = np.append(y_train, label_num)

x_train = np.delete(x_train, (0), axis=0)  # delete the empty row

np.save('x_train'+str(IMG_SIZE), x_train)
np.save('y_train'+str(IMG_SIZE), y_train)


